package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
)

func SendMessage(producer *sarama.Producer, message string) {
	producer.SendMessage("my_topic", nil, sarama.StringEncoder(message))
}

func CreateProducer() *sarama.Producer {
	broker := createBroker()
	client := createClient(broker)
	producer := createProducer(client)

	return producer
}

func createBroker() *sarama.Broker {
	mb := sarama.NewBroker("169.254.101.81:9092")

	mdr := new(sarama.MetadataResponse)
	mdr.AddBroker(mb.Addr(), mb.ID())
	mdr.AddTopicPartition("my_topic", 0, 1)

	return mb
}

func createClient(mb *sarama.Broker) *sarama.Client {
	client, err := sarama.NewClient("client_id", []string{mb.Addr()}, nil)

	if err != nil {
		fmt.Println(err)
	}

	return client
}

func createProducer(client *sarama.Client) *sarama.Producer {
	producer, err := sarama.NewProducer(client, nil)

	if err != nil {
		fmt.Println(err)
	}

	return producer
}
