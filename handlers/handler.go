package handlers

import (
	"code.google.com/p/go-uuid/uuid"
	"fmt"
	"github.com/xebia/micro-chat-users/domain"
	"github.com/xebia/micro-chat-users/kafka"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

const DbUrl = "localhost"

func UsersPostHandler(vars map[string]string, body interface{}, request *http.Request) interface{} {
	user := body.(domain.User)
	foundUser := findUser(user.Username)
	if foundUser == nil {
		saveUser(user)
	}
	return ""
}

func UsersLoginPostHandler(vars map[string]string, body interface{}, request *http.Request) interface{} {
	credentials := body.(domain.Credentials)
	foundUser := findUser(credentials.Username)
	if foundUser != nil && foundUser.Password == credentials.Password {
		fmt.Printf("Posting object: %s\n", credentials)
		token := domain.Session{}
		token.Token = uuid.NewRandom().String()
		fmt.Printf("token: %s", token)
		sendMessageToQueue(credentials.Username, token.Token)
		return token
	} else {
		fmt.Println("username / password incorrect")

	}
	return ""
}

func saveUser(user domain.User) {
	session, err := mgo.Dial(DbUrl)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("users")

	user.Id = bson.NewObjectId()

	fmt.Println("%s", user)
	err = c.Insert(user)
	if err != nil {
		log.Fatal(err)
	}

	result := domain.User{}
	err = c.FindId(user.Id).One(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}

func findUser(username string) *domain.User {
	session, err := mgo.Dial(DbUrl)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("users")

	result := domain.User{}
	query := c.Find(bson.M{"username": username})

	n, err := query.Count()
	if err != nil {
		log.Fatal(err)
	}
	if n == 0 {
		return nil
	} else {
		query.One(&result)
		return &result
	}
}

func sendMessageToQueue(username string, token string) {
	// Message: event,username,token
	fmt.Println("Create Kafka producer")
	producer := kafka.CreateProducer()
	// Message: event,username,token
	kafka.SendMessage(producer, "UserLoggedIn,"+username+","+token)
	fmt.Println("Kafka producer produced")
}
