package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type User struct {
	Id       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Email    string
	Username string
	Password string
}

func main() {
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("users")

	user := &User{bson.NewObjectId(), "awulder@xebia.com", "awulder", "Welkom01"}
	err = c.Insert(user)
	if err != nil {
		log.Fatal(err)
	}

	result := User{}
	err = c.FindId(user.Id).One(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}
