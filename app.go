package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/xebia/micro-chat-users/domain"
	"github.com/xebia/micro-chat-users/handlers"
	"io/ioutil"
	"net/http"
)

func main() {
	fmt.Printf("Server started on http://localhost:8000\n")

	usersRouter := mux.NewRouter()
	usersRouter.HandleFunc("/users", asJsonWithBody(handlers.UsersPostHandler, domain.User{})).Methods("POST")
	usersRouter.HandleFunc("/users/login", asJsonWithBody(handlers.UsersLoginPostHandler, domain.Credentials{})).Methods("POST")
	http.Handle("/users", usersRouter)
	http.Handle("/users/login", usersRouter)
	http.Handle("/", http.FileServer(http.Dir("./static")))

	http.ListenAndServe(":8000", nil)
}

func asJsonWithBody(handler func(map[string]string, interface{}, *http.Request) interface{}, bodyType interface{}) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)

		body, _ := ioutil.ReadAll(request.Body)

		switch i := bodyType.(type) {
		case domain.User:
			userWrapper := domain.User{}
			json.Unmarshal(body, &userWrapper)
			bodyType = userWrapper
		case domain.Credentials:
			credentialsWrapper := domain.Credentials{}
			json.Unmarshal(body, &credentialsWrapper)
			bodyType = credentialsWrapper
		default:
			json.Unmarshal(body, &i)
			bodyType = i
		}

		data := handler(vars, bodyType, request)

		bytes, err := json.Marshal(data)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		writer.Header().Set("Content-Type", "application/json; charset=utf-8")
		writer.Write(bytes)
	}
}
